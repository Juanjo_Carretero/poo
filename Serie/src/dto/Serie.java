package dto;

public class Serie {
	
	//ATRIBUTO
	private String titulo;
	
	private int num_temporadas;
	
	private boolean entregado;
	
	private String genero;
	
	private String creador;
	
	//CONSTRUCTORES
	//GENERO EL CONSTRUCTOR POR DEFECTO
	public Serie() {
		this.creador="";
		this.titulo="";
		this.num_temporadas=3;
		this.entregado=false;
		this.genero="";
	}
	
	public Serie(String creador, String titulo) {
		this.creador=creador;
		this.titulo=titulo;
		this.num_temporadas=3;
		this.entregado=false;
		this.genero="";
	}
	
	public Serie(String creador, String titulo, int num_temporadas, String genero) {
		this.creador=creador;
		this.titulo=titulo;
		this.num_temporadas=num_temporadas;
		this.genero=genero;
	}
}
