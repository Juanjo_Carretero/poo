package dto;

import java.security.SecureRandom;

import java.math.BigInteger;

public class Password {
	//ATRIBUTOS
	
	private int longitud;
	
	private String contraseņa;
	
	//GENERO EL CONSTRUCTOR POR DEFECTO
	public Password() {
		this.longitud=8;
		this.contraseņa="";
	}
	
	public String generocontra(int longitud) {
		SecureRandom random = new SecureRandom();
		
		String randompass = new BigInteger(longitud, random).toString(32);
		
		return randompass;
		
	}
	
	
}
