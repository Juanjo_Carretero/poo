package dto;

import javax.swing.JOptionPane;

public class Electrodomestico {
	
	//CONSTANTES
	final int PRECIO_BASE = 100;
	
	final String COLOR = "blanco";
	
	final char CONSUMO = 'F';
	
	final double PESO = 5;
	
	//ATRIBUTOS
	private int precio_base;//PRECIO BASE DEL ELECTRODOMESTICO
	
	private String color;//COLOR DEL ELECTRODOMESTICO
	
	private char consumo;//CONSUMO DEL ELECTRODOMESTICO
	
	private double peso;//PESO DEL ELECTRODOMESTICO
	
	//CONSTRUCTORES
	//GENERO EL CONSTRUCTOR POR DEFECTO
	public Electrodomestico() {
		this.precio_base=PRECIO_BASE;
		this.color=COLOR;
		this.peso=PESO;
		this.consumo=CONSUMO;
	}
	
	public Electrodomestico(int precio_base, double peso) {
		this.precio_base=precio_base;
		this.peso=peso;
		this.color=COLOR;
		this.consumo=CONSUMO;
	}
	
	public Electrodomestico(int precio_base, double peso, String color, char consumo) {
		this.precio_base=precio_base;
		this.color=seleccionColor(color);
		this.peso=peso;
		this.consumo=seleccionConsumo(consumo);
	}
	
	public static String seleccionColor(String color) {
		
		String disponible="";
		
		if (color.equalsIgnoreCase("blanco")) {
			disponible = "blanco";
		}else if (color.equalsIgnoreCase("negro")) {
			disponible = "negro";
		}else if (color.equalsIgnoreCase("rojo")) {
			disponible = "rojo";
		}else if (color.equalsIgnoreCase("azul")) {
			disponible = "azul";
		}else if (color.equalsIgnoreCase("gris")) {
			disponible = "gris";
		}else {
			disponible = "blanco";
		}
		
		return disponible;
	}
	
	public static char seleccionConsumo(char consumo) {
		
		switch (consumo) {
		case 'A':
			consumo = 'A';
		break;
		
		case 'B':
			consumo = 'B';
		break;
		
		case 'C':
			consumo = 'C';
		break;
		
		case 'D':
			consumo = 'D';
		break;
		
		case 'E':
			consumo = 'E';
		break;
		
		case 'F':
			consumo = 'F';
		break;
		
		default:
			consumo = 'F';
		break;
		}
		
		
		return consumo;
	}
}
