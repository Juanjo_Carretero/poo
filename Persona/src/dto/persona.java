package dto;

public class persona {
	//ATRIBUTOS

	final char sx='H';
	
	private String nombre;//NOMBRE DE LA PERSONA
	
	private int edad;//EDAD DE LA PERSONA
	
	private String dni;//DNI DE LA PERSONA
	
	private char sexo;//SEXO DE LA PERSONA
	
	private double peso;//PESO DE LA PERSONA
	
	private double altura;//ALTURA DE LA PERSONA
	
	//CONSTRUCTORES
	//GENERO EL CONSTRUCTOR POR DEFECTO
	public persona() {
		this.nombre="";
		this.edad=0;
		this.dni=39999999F;
		this.sexo=sx;
		this.peso=0;
		this.altura=0;
	}
	
	public persona(String nombre, int edad, char sexo) {
		this.nombre=nombre;
		this.edad=edad;
		this.sexo=sexo;
	}
	
	public persona(String nombre, int edad, char sexo, double peso, double altura) {
		this.nombre=nombre;
		this.edad=edad;
		this.sexo=sexo;
		this.peso=peso;
		this.altura=altura;
	}
}
